import { Component } from '@angular/core';

@Component({
  selector: 'app-default-layout',
  styleUrls: ['./default.layout.scss'],
  templateUrl: './default.layout.html'
})
export class DefaultLayoutComponent {
  constructor() {}
}
