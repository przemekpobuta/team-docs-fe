import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification-popover',
  templateUrl: './notification-popover.component.html',
  styleUrls: ['./notification-popover.component.scss']
})
export class NotificationPopoverComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  notifications = [
    'Kuba polubił Twojego posta',
    'Michał obserwuje Twój profil'
  ];
}
