import { Component, OnInit } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { NbAuthService, NbAuthResult } from '@nebular/auth';
import { NotificationPopoverComponent } from '../notification-popover/notification-popover.component';
import { UserService } from '../../../core/auth/user.service';
import { User } from '../../../shared/models/user.model';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  constructor(
    private _sidebarService: NbSidebarService,
    private _menuService: NbMenuService,
    public _authService: NbAuthService,
    private _userService: UserService
  ) {}

  isAuthenticated = false;
  user: any;

  userMenu = [
    { title: 'Log out', icon: 'log-out-outline', link: '/auth/logout' }
  ];

  notificationPopoverComponent = NotificationPopoverComponent;

  ngOnInit() {
    this.reqIsAuthenticated();

    this._userService.getUserData().subscribe(
      (user: User) => {
        this.user = {
          name: user.fullName,
          picture: user.pictureUrl
        };
      },
      err => {}
    );
  }

  reqIsAuthenticated() {
    this._authService
      .isAuthenticated()
      .subscribe((isAuthenticated: boolean) => {
        this.isAuthenticated = isAuthenticated;
      });
  }

  toggleSidebar(): boolean {
    this._sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  goToHome() {
    this._menuService.navigateHome();
  }

  onLogout() {
    this._authService.logout('email').subscribe((data: NbAuthResult) => {
      console.log(data.getMessages);
      this.reqIsAuthenticated();
    });
  }
}
