import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-template-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicTemplateComponent implements OnInit {
  @Input() title: string;

  constructor() {}

  ngOnInit() {}
}
