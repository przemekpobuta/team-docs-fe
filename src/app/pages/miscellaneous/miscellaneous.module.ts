import { NgModule } from '@angular/core';

import { MiscellaneousComponent } from './miscellaneous.component';
import {
  MiscellaneousRoutingModule,
  routedComponents
} from './miscellaneous-routing.module';
import { ThemeModule } from '../../theme/theme.module';

@NgModule({
  declarations: [MiscellaneousComponent, ...routedComponents],
  imports: [ThemeModule, MiscellaneousRoutingModule]
})
export class MiscellaneousModule {}
