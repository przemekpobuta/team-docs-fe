import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { NbAuthService } from '@nebular/auth';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  isAuthencticated = false;
  menuAuth: NbMenuItem[] = [
    {
      title: 'Dashboard',
      icon: 'home-outline',
      link: '/pages/dashboard',
      home: true
    },
    {
      title: 'Docs',
      icon: 'file-text-outline',
      link: '/pages/docs',
      pathMatch: '/pages/docs'
    }
  ];

  menuNoAuth: NbMenuItem[] = [
    {
      title: 'Dashboard',
      icon: 'home-outline',
      link: '/pages/dashboard',
      home: true
    },
    {
      title: 'Login',
      icon: 'unlock-outline',
      link: '/auth/login'
    },
    {
      title: 'Register',
      icon: 'unlock-outline',
      link: '/auth/register'
    }
  ];

  constructor(private _nbAuthService: NbAuthService) {}

  ngOnInit(): void {
    this._nbAuthService.isAuthenticated().subscribe(res => {
      this.isAuthencticated = res;
    });
  }
}
