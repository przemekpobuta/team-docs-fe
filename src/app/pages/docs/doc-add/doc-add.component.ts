import { Component, OnInit } from '@angular/core';
import { DocsService } from '../docs.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { Doc } from '../models/doc.model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-doc-add',
  templateUrl: './doc-add.component.html',
  styleUrls: ['./doc-add.component.scss']
})
export class DocAddComponent implements OnInit {
  addDocFormGroup: FormGroup;

  public Editor = ClassicEditor;
  editorConfig = {
    placeholder: 'Type the content here...'
  };

  constructor(
    private _docsService: DocsService,
    private _formBuilder: FormBuilder,
    private _nbToastrService: NbToastrService,
    private _router: Router
  ) {}

  ngOnInit() {
    this.addDocFormGroup = this._formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  onSubmit() {
    console.log(this.addDocFormGroup.value);

    const docFormValues: Doc = this.addDocFormGroup.value;

    this._docsService.createDoc(docFormValues).subscribe(
      (res: Doc) => {
        console.log(res);
        this._nbToastrService.primary('Successfuly created document!');
        this._router.navigate(['pages/docs/view', res._id]);
      },
      err => {
        console.error(err);
        this._nbToastrService.danger(err);
      }
    );
  }
}
