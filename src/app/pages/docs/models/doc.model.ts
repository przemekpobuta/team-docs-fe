import { User } from '../../../shared/models/user.model';

export interface Doc {
  _id?: string;
  title: string;
  content: string;
  created_by: User;
  created_date?: string;
  updated_date?: string;
}
