import { Component, OnInit } from '@angular/core';
import { DocsService } from '../docs.service';
import { Doc } from '../models/doc.model';
import { NbToastrService } from '@nebular/theme';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-docs-list',
  templateUrl: './docs-list.component.html',
  styleUrls: ['./docs-list.component.scss']
})
export class DocsListComponent implements OnInit {
  loading = false;
  docs: Doc[];
  getDocListReq: Subscription;

  constructor(
    private _docService: DocsService,
    private _nbToastrService: NbToastrService
  ) {}

  ngOnInit() {
    this.getDocList();
  }

  getDocList() {
    this.loading = true;
    this.getDocListReq = this._docService.getDocsList().subscribe(
      (data: Doc[]) => {
        console.log(data);
        this.docs = data;
      },
      err => console.error(err),
      () => {
        this.loading = false;
      }
    );
  }

  onDelete(doc: Doc) {
    this._docService.deleteDoc(doc._id).subscribe(
      res => {
        console.log(res);
        this._nbToastrService.primary(res['message']);
        this.getDocList();
      },
      err => {
        this._nbToastrService.danger(err);
      }
    );
  }
}
