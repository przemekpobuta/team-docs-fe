import { Component, OnInit, OnDestroy } from '@angular/core';
import { DocsService } from '../docs.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { ActivatedRoute } from '@angular/router';
import { Doc } from '../models/doc.model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-doc-edit',
  templateUrl: './doc-edit.component.html',
  styleUrls: ['./doc-edit.component.scss']
})
export class DocEditComponent implements OnInit, OnDestroy {
  getDocSub: Subscription;
  updateDocSub: Subscription;
  routeSub: Subscription;
  addDocFormGroup: FormGroup;
  doc: Doc;
  loading = false;
  docId: string;

  public Editor = ClassicEditor;
  editorConfig = {
    placeholder: 'Type the content here...'
  };

  constructor(
    private _docsService: DocsService,
    private _formBuilder: FormBuilder,
    private _nbToastrService: NbToastrService,
    private _route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.routeSub = this._route.params.subscribe(params => {
      const docID = params['id'];
      this.docId = docID;
      this.getDocReq();
    });

    this.addDocFormGroup = this._formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    if (this.getDocSub) {
      this.getDocSub.unsubscribe();
    }
    if (this.updateDocSub) {
      this.updateDocSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

  getDocReq() {
    this.loading = true;
    this.getDocSub = this._docsService.getDoc(this.docId).subscribe(
      (doc: Doc) => {
        this.doc = doc;
        this.setForm(doc);
      },
      err => {
        this._nbToastrService.danger(err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  setForm(doc: Doc) {
    console.log('setForm', doc);
    this.addDocFormGroup.get('title').setValue(doc.title);
    this.addDocFormGroup.get('content').setValue(doc.content);
  }

  onSubmit() {
    console.log(this.addDocFormGroup.value);
    this.loading = true;

    const docFormValues: Doc = this.addDocFormGroup.value;
    console.log('docFormValues', docFormValues);
    console.log('docId', this.docId);

    this.updateDocSub = this._docsService
      .updateDoc(this.docId, docFormValues)
      .subscribe(
        (res: Doc) => {
          console.log('updateDoc', res);
          this.getDocReq();
          this._nbToastrService.primary('Successfuly saved document!');
        },
        err => {
          console.error(err);
          this._nbToastrService.danger(err);
        },
        () => {
          this.loading = false;
        }
      );
  }
}
