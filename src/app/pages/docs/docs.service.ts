import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Doc } from './models/doc.model';
import { runInThisContext } from 'vm';

@Injectable({
  providedIn: 'root'
})
export class DocsService {
  private apiUrl = environment.apiUrl;
  private docUrl = this.apiUrl + 'doc/';

  constructor(private _http: HttpClient) {}

  getDocsList() {
    return this._http.get<Doc[]>(this.docUrl);
  }

  getDoc(docId: string) {
    return this._http.get<Doc>(this.docUrl + docId);
  }

  createDoc(doc: Doc) {
    return this._http.post<Doc>(this.docUrl, doc);
  }

  updateDoc(docId: string, doc: Doc) {
    return this._http.put<Doc>(this.docUrl + docId, doc);
  }

  deleteDoc(docId: string) {
    return this._http.delete(this.docUrl + docId);
  }
}
