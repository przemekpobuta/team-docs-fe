import { Component, OnInit, OnDestroy } from '@angular/core';
import { DocsService } from '../docs.service';
import { NbToastrService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { Doc } from '../models/doc.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-doc-view',
  templateUrl: './doc-view.component.html',
  styleUrls: ['./doc-view.component.scss']
})
export class DocViewComponent implements OnInit, OnDestroy {
  loading = false;
  doc: Doc;
  getDocSub: Subscription;
  private routeSub: Subscription;

  constructor(
    private _docService: DocsService,
    private _nbToastrService: NbToastrService,
    private _route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.routeSub = this._route.params.subscribe(params => {
      this.getDoc(params['id']);
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  getDoc(id: string) {
    this.loading = true;
    this.getDocSub = this._docService.getDoc(id).subscribe(
      (data: Doc) => {
        console.log(data);
        this.doc = data;
      },
      err => console.error(err),
      () => {
        this.loading = false;
      }
    );
  }

  onDelete() {
    this._docService.deleteDoc(this.doc._id).subscribe(
      res => {
        console.log(res);
        this._nbToastrService.primary(res['message']);
        // redirect
      },
      err => {
        this._nbToastrService.danger(err);
      }
    );
  }
}
