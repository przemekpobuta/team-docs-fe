import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DocsComponent } from './docs.component';
import { DocsListComponent } from './docs-list/docs-list.component';
import { DocViewComponent } from './doc-view/doc-view.component';
import { DocAddComponent } from './doc-add/doc-add.component';
import { DocEditComponent } from './doc-edit/doc-edit.component';

const routes: Routes = [
  {
    path: '',
    component: DocsComponent,
    children: [
      {
        path: 'list',
        component: DocsListComponent
      },
      {
        path: 'view/:id',
        component: DocViewComponent
      },
      {
        path: 'add',
        component: DocAddComponent
      },
      {
        path: 'edit/:id',
        component: DocEditComponent
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocsRoutingModule {}
