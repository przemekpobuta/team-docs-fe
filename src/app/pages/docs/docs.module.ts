import { NgModule } from '@angular/core';
import { DocsComponent } from './docs.component';
import { DocsRoutingModule } from './docs-routing.module';
import { DocsListComponent } from './docs-list/docs-list.component';
import { DocViewComponent } from './doc-view/doc-view.component';
import { DocAddComponent } from './doc-add/doc-add.component';
import { DocEditComponent } from './doc-edit/doc-edit.component';
import { ThemeModule } from '../../theme/theme.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  declarations: [
    DocsComponent,
    DocsListComponent,
    DocViewComponent,
    DocAddComponent,
    DocEditComponent
  ],
  imports: [DocsRoutingModule, ThemeModule, CKEditorModule]
})
export class DocsModule {}
