import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ThemeModule } from '../../theme/theme.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [DashboardComponent, WelcomeComponent],
  imports: [ThemeModule, CommonModule, DashboardRoutingModule]
})
export class DashboardModule {}
