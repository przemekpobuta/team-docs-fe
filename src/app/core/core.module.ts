import {
  NgModule,
  Optional,
  SkipSelf,
  ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { of as observableOf } from 'rxjs';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { NbRoleProvider, NbSecurityModule } from '@nebular/security';
import {
  NbAuthModule,
  NbPasswordAuthStrategy,
  NbAuthJWTToken,
  NbAuthService,
  NbAuthJWTInterceptor
} from '@nebular/auth';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { AuthGuard } from './guards/auth-guard.service';
import { UserService } from './auth/user.service';

const DATA_SERVICES = [
  // { provide: UserData, useClass: UserService }
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const CORE_PROVIDERS = [
  // single instance services
  ...DATA_SERVICES,
  ...NbAuthModule.forRoot({
    strategies: [
      NbPasswordAuthStrategy.setup({
        name: 'email',
        baseEndpoint: environment.apiUrl + 'auth/',
        token: {
          class: NbAuthJWTToken,
          key: 'token'
        },
        login: {
          endpoint: 'login',
          method: 'post'
        },
        register: {
          endpoint: 'register',
          method: 'post'
        },
        logout: {
          endpoint: 'logout',
          method: 'post'
        }
        // requestPass: {
        //   endpoint: '/auth/request-pass',
        // },
        // resetPass: {
        //   endpoint: '/auth/reset-pass',
        // }
      })
    ],
    forms: {
      login: {
        showMessages: {
          success: true,
          error: true
        }
      }
    }
  }).providers,
  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*'
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*'
      }
    }
  }).providers,
  {
    provide: NbRoleProvider,
    useClass: NbSimpleRoleProvider
  },
  AuthGuard,
  UserService
];

@NgModule({
  imports: [CommonModule],
  exports: [NbAuthModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ],
  declarations: []
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [...CORE_PROVIDERS]
    };
  }
}
