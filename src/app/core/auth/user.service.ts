import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl = environment.apiUrl;
  private authUrl = this.apiUrl + 'auth/';

  constructor(private _http: HttpClient) {}

  getUserData() {
    return this._http.get(this.authUrl + 'me');
  }
}
