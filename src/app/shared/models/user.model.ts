export interface User {
  _id: string;
  email?: string;
  fullName: string;
  pictureUrl?: string;
}
